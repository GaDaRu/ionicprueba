import { Component } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.page.html',
  styleUrls: ['./admin.page.scss'],
  /*template: `
    <div>
      <h2>{{ 'BORRART' | translate }}</h2>
      <label>
        {{ 'FECHA' | translate }}
        <select #langSelect (change)="translate.use(langSelect.value)">
          <option *ngFor="let lang of translate.getLangs()" [value]="lang" [selected]="lang === translate.currentLang">{{ lang }}</option>
        </select>
      </label>
    </div>
  `,*/
})
export class AdminPage {

  public cerrar: string;
  public seguro: string;
  public si: string;

  constructor(private authSvc: AuthService, private router: Router, public translate: TranslateService) {
  }

  // Cargamos las traducciones
  public ionViewDidEnter(){
    this.translate.get('CERRAR').subscribe((res: string) => {this.cerrar = res; });
    this.translate.get('ESTAS').subscribe((res: string) => {this.seguro = res; });
    this.translate.get('SI').subscribe((res: string) => {this.si = res; });
  }

  // Funcion para desloguearse
  async exit(){
    await this.authSvc.logout();
    this.router.navigate(['/login']);
  }

  // Al darle a salir de la app nos aparecerá un alert para confirmar
  public confirm() {
    const alert = document.createElement('ion-alert');

    alert.header =  this.cerrar,
      alert.message = this.seguro,
      alert.buttons =
        [{
          text: this.si,
          handler: () => {
            this.exit();
          }
        }, {
          text: 'NO',
          role: 'cancelar'
        }];
    document.querySelector('ion-app').appendChild(alert);
    alert.present();
  }
}
