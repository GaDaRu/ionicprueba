import { Component } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage{

  constructor(private authSVC: AuthService, private router: Router) { }

  // Metodo para registrar un usuario
  async onRegister(email, password){
    try {
      const user = await this.authSVC.register(email.value, password.value);
      if (user){
        const isVerified = this.authSVC.isEmailVerified(user);
        this.redirectUser(isVerified);
      }
    }catch (error){
      console.log(error);
    }
  }

  // tslint:disable-next-line:max-line-length
  // En este metodo comprobamos si el correo está verificado, en caso afirmativo lo llevamos a la vista principal, en caso negativo lo llevamos a la ventana de verificacion
  private redirectUser(isVerified: boolean){
    if (isVerified){
      this.router.navigate(['admin']);
    }else{
      this.router.navigate(['verify-email']);
    }
  }
}
