import { Component } from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  public language = 'es';

  constructor(private authSvc: AuthService, private router: Router, public translate: TranslateService) { }

  // Metodo para loguearse un usuario
  async onLogin(email, password){
    try{
      const user = await this.authSvc.login(email.value, password.value);
      if (user){
        const isVerify = this.authSvc.isEmailVerified(user);
        console.log('Veri ->', isVerify);
        this.redirectUser(isVerify);
      }
    }catch (error){
      console.log(error);
    }
  }

  // Logueo con Google
  async onLoginGoogle(){
    try {
      const user = await this.authSvc.loginGoogle();
      if (user){
        const isVerify = this.authSvc.isEmailVerified(user);
        console.log('Veri ->', isVerify);
        this.redirectUser(isVerify);
      }
    }catch (error){
      console.log(error);
    }
  }

  // Redireccion a ventana principal o verificacion
  private redirectUser(isVerified: boolean){
    if (isVerified){
      this.router.navigate(['/admin']);
    }else{
      this.router.navigate(['/verify-email']);
    }
  }

  public getLanguage(){
    return this.language;
  }

  public setLanguage(language){
    this.language = language;
  }
}
