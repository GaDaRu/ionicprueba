import { Component } from '@angular/core';
import {Router} from '@angular/router';
import {CallNumber} from '@ionic-native/call-number/ngx';


@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page {

  constructor(private callNumber: CallNumber, private router: Router) { }

  // Cambiamos el numero
  public setResult(value) {
    document.getElementById('number').innerHTML = value;
  }

  // Obtenemos el numero
  public getResult() {
    return(document.getElementById('number').innerHTML);
  }

  // Añadimos un numero
  public add(key){
    const result = this.getResult();
    if (result !== '0' || isNaN(key)) { this.setResult(result + key); }
    else { this.setResult(key); }
  }

  // Borramos el ultimo numero añadido, en caso de que borremos todos los numeros aparecerá un 0
  public delete(){
    let number2 = this.getResult();
    if (number2 === '0'){}
    else{
      number2 = number2.substring(0, number2.length - 1);
      if (number2.length === 0){
        number2 = '0';
      }
    }
    this.setResult(number2);
  }

  // Funcion para realizar la llamada
  public call(){
    this.callNumber.callNumber(this.getResult(), true)
      .then(res => console.log('Llamada realizada!', res))
      .catch(err => console.log('Error en la llamada', err));
    // window.open('tel:' + this.getResult(), '_system');
    this.setResult(0);
    // this.router.navigate(['/admin']);
  }
}
