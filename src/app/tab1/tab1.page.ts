import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: './tab1.page.html',
  styleUrls: ['./tab1.page.scss'],
})
export class Tab1Page {

  constructor() { }

  // Cambiar el valor
  public setResult(value) {
    document.getElementById('result').innerHTML = value;
  }

  // Obtener el valor a calcular
  public getResult() {
    return(document.getElementById('result').innerHTML);
  }

  // Añadir el nuevo numero pulsado
  public add(key) {
    const result = this.getResult();
    if (result !== '0' || isNaN(key)) { this.setResult(result + key); }
    else { this.setResult(key); }
  }

  // Calcular el resultado y mostrarlo
  public calc() {
    const result = eval(this.getResult());
    this.setResult(result);
  }

  // Borramos todos los numeros
  public del() {
    this.setResult(0);
  }

  // Borramos el ultimo numero añadido
  public delete(){
    let number2 = this.getResult();
    if (number2 === '0'){}
    else{
      number2 = number2.substring(0, number2.length - 1);
      if (number2.length === 0){
        number2 = '0';
      }
    }
    this.setResult(number2);
  }
}
