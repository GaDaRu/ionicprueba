import { Component } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-tab3',
  templateUrl: './tab3.page.html',
  styleUrls: ['./tab3.page.scss'],
})
export class Tab3Page {
  public list = [];
  public tarea: string;
  public NT: string;
  public MT: string;
  public fecha: string;
  public anadirT: string;
  public prioridad: string;
  public mensaje: string;
  public bta: string;
  public bto: string;
  public seguro: string;
  public si: string;

  constructor(public translate: TranslateService) {}

  // Aqui estamos cargando las traducciones y la lista al iniciar
  public ionViewDidEnter(){
    this.getList();
    this.translate.get('TAREA').subscribe((res: string) => {this.tarea = res; });
    this.translate.get('NT').subscribe((res: string) => {this.NT = res; });
    this.translate.get('MT').subscribe((res: string) => {this.MT = res; });
    this.translate.get('FECHA').subscribe((res: string) => {this.fecha = res; });
    this.translate.get('ANADIRTAREA').subscribe((res: string) => {this.anadirT = res; });
    this.translate.get('PRIORIDAD').subscribe((res: string) => {this.prioridad = res; });
    this.translate.get('MENSAJE').subscribe((res: string) => {this.mensaje = res; });
    this.translate.get('BORRARTA').subscribe((res: string) => {this.bta = res; });
    this.translate.get('BORRART').subscribe((res: string) => {this.bto = res; });
    this.translate.get('ESTAS').subscribe((res: string) => {this.seguro = res; });
    this.translate.get('SI').subscribe((res: string) => {this.si = res; });
  }

  // En caso de salir dde la app borramos la lista
  public ionViewWillLeave(){
    this.list.length = 0;
  }

  // Obtenermos el TAB
  public getTab() {
    return(document.querySelector('ion-content:not(.tab-hidden)'));
  }

  // Obtenemos la lista guardada en el sistema
  public getList(tab = this.getTab()) {
    const list = localStorage.getItem('whole-list-' + tab);
    this.list = list ? JSON.parse(list) : [];
    console.log(this.list);
  }

  // Guardamos la lista en el sistema
  public saveList(tab, list) {
    localStorage.setItem('whole-list-' + tab, JSON.stringify(list));
  }

  // Mensaje de error
  public error(message) {
    const alert = document.createElement('ion-alert');
    alert.message = message;
    alert.buttons = ['OK'];
    document.querySelector('ion-app').appendChild(alert);
    alert.present();
  }

  // Aqui nos aparecerá una vista donde podremos añadir ó modificar una tarea
  public addEditItem(index) {
    const list = this.list;
    let item = null;
    const aa = {home: '', settings: ''};
    if (index >= 0) {
      item = list[ index ];
    } else {
      item = {
        text: '',
        date: new Date().toISOString(),
        icon: 'checkbox-outline' };
    }
    const modal = document.createElement('ion-modal');
    modal.component = document.createElement('div');
    modal.component.innerHTML = `
        <ion-header>
            <ion-toolbar>
                <ion-title>` + (this.tarea) + (index >= 0 ? (this.NT) : (this.MT)) + `</ion-title>
                <ion-buttons slot="primary">
                    <ion-button color="danger"><ion-icon slot="icon-only" name="close"></ion-icon></ion-button>
                    <ion-button color="primary"><ion-icon slot="icon-only" name="checkmark"></ion-icon></ion-button>
                </ion-buttons>
            </ion-toolbar>
        </ion-header>
        <ion-content>
            <ion-list>
                <ion-item>
                    <ion-label position="floating">` + (this.fecha) + `</ion-label>
                    <ion-datetime display-format="D MMM YYYY" max="2050-12-31" value="` + item.date + `"></ion-datetime>
                </ion-item>
                <ion-item>
                    <ion-label position="floating">` + (this.anadirT) + `</ion-label>
                    <ion-input value="` + item.text + `"></ion-input>
                </ion-item>
            </ion-list>
            <br>
            <ion-text style="text-align: center">` + (this.prioridad) + `</ion-text>
            <ion-segment value="` + item.icon + `">
                <ion-segment-button value="checkbox-outline">
                    <ion-icon name="checkbox-outline"></ion-icon>
                </ion-segment-button>
                <ion-segment-button value="alert-circle-outline">
                    <ion-icon name="alert-circle-outline"></ion-icon>
                </ion-segment-button>
                <ion-segment-button value="alert-outline">
                    <ion-icon name="alert-outline"></ion-icon>
                </ion-segment-button>
            </ion-segment>
        </ion-content>`;
    modal.component.querySelector('[color="danger"]').addEventListener('click', () => {
      modal.dismiss();
    });
    modal.component.querySelector('[color="primary"]').addEventListener('click', () => {
      const newDate = document.querySelector('ion-datetime').value;
      const newText = document.querySelector('ion-input').value;
      const newIcon = document.querySelector('ion-segment').value;
      let newColor = '';
      if (newIcon === 'alert-outline'){
        // tslint:disable-next-line:no-unused-expression
        newColor = 'red';
      } else if (newIcon === 'alert-circle-outline'){
        newColor = '#FFC107';
      } else if (newIcon === 'checkbox-outline'){
        newColor = 'green';
      }
      console.log('Color: ' + newColor);
      // @ts-ignore
      if (!newText.length) {
        this.error(this.mensaje);
      }
      else {
        const newItem = { text: newText, date: newDate, icon: newIcon, color: newColor };
        if (index >= 0) {
          this.list[index] = newItem;
        }
        else {
          this.list.unshift(newItem);
        }
        this.saveList(this.getTab(), this.list);
        modal.dismiss();
      }
    });
    document.querySelector('ion-app').appendChild(modal);
    modal.present();
  }

  // Borramos todas las tareas
  public deleteAllItem(index = false) {
    const alert = document.createElement('ion-alert');

    alert.header = index !== false ? this.bta : this.bto,
      alert.message = this.seguro,
      alert.buttons =
        [{
          text: this.si,
          handler: () => {
            this.list.length = 0;
            this.saveList(this.getTab(), this.list);
          }
        }, {
          text: 'NO',
          role: 'cancelar'
        }];
    document.querySelector('ion-app').appendChild(alert);
    alert.present();
  }

  // Borramos una tarea
  public deleteItem(index) {
    const alert = document.createElement('ion-alert');

    alert.header = index !== false ? this.bta : this.bto,
      alert.message = this.seguro,
      alert.buttons =
        [{
          text: this.si,
          handler: () => {
            this.list.splice(index, 1);
            this.saveList(this.getTab(), this.list);
          }
        }, {
          text: 'NO',
          role: 'cancelar'
        }];
    document.querySelector('ion-app').appendChild(alert);
    alert.present();
  }
}
